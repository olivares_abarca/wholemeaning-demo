import styled from "styled-components";

export const Table = styled.table`
  --border-radius: 7px;
  --border-size: 1px;
  --padding-size: 10px;

  //width: 300px;
  //height: 400px;
  border-collapse: collapse;
  border-spacing: 0;
  border-style: hidden;
  th {
    padding: var(--padding-size);
    border-right: var(--border-size) solid #C9CFD2;
    background-color: #1D837A;
    font-family: 'Archivo Medium';
    font-size: 80%;
    font-weight: normal;
    color: white;
    text-align: center;
  }

  td {
    padding: var(--padding-size);
    border-right: var(--border-size) solid #C9CFD2;
    background-color: #EEEEEE;
    //font-family: 'Open Sans Light';
    font-size: 75%;
    font-weight: normal;
    color: black;
    text-align: left;
    word-break: normal;
    height: 290px;
  }

  /* Top-Right Border */
  tr:first-child th:last-child {
    border-top-right-radius: var(--border-radius);
  }

  /* Bottom-Right Border */
  tr:last-child td:last-child {
    border-bottom-right-radius: var(--border-radius);
  }

  /* Top-Left Border */
  tr:first-child th:first-child {
    border-top-left-radius: var(--border-radius);
  }

  /* Bottom-Left Border */
  tr:last-child td:first-child {
    border-bottom-left-radius: var(--border-radius);
  }
`;