import * as Styled from './styles.jsx';

const Table = (props) => {

  const category = props.category === '' ? 'No se ha seleccionado un requerimiento' : props.category;
  const template = props.template === '' ? 'No se ha seleccionado un requerimiento' : props.template;

  return (
    <Styled.Table>
      <tr>
        <th>Categoría</th>
        <th>Automatización</th>
      </tr>
      <tr>
        <td>{category}</td>
        <td>{category == template ? '' :template}</td>
      </tr>
    </Styled.Table>
  );
};

export default Table;