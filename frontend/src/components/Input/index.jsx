import * as Styled from './styles.jsx';

const Input = (props) => {
  let data = props.template;

  const onChange = (val) => {
    props.updateContent(val);
  }
  
  return (
    <div>
      <Styled.Textarea>
        <textarea value={data} onChange={(e) => onChange(e.target.value)} placeholder='Selecciona un requerimiento o escribe uno...'></textarea>
      </Styled.Textarea>
    </div>
  );
}

export default Input;