import styled, { css } from 'styled-components';

const textarea = css`
  textarea {
    position: relative;
    display: block;
    height: 180px;
    box-sizing: border-box;
    padding: 0;
    border: none;
    outline: none;
    font-size: 75%;
    color: #170A1C;
    resize: none;
    scrollbar-width: none;
    background-color: #ECF0F1;

    &::-webkit-scrollbar {
      display: none;
    }
  }
`;

const container = css`
  padding: 30px;
  border-radius: 10px;
  box-sizing: border-box;
  background-color: #ECF0F1;
  ${textarea}
`;

export const Textarea = styled.div`
  ${container}
  box-sizing: border-box;
  button {
    position: relative;
  }
`;