import * as Styled from './styles.jsx';

const List = (props) => {
  const data = props.items;
  const visisbleData = props.items.filter(item => item.sector === props.sector);
  const items = visisbleData.map(item => <Styled.Li key={item.id} selected={item.selected} onClick={() => handleClick(item.id)}>{item.message}</Styled.Li>);

  const handleClick = (selected_id) => {
    data.forEach(item => {
      if (item.id === selected_id)
        item.selected = true;
      else
        item.selected = false;
    });
    props.updateItems([...data]);
  }

  return (
    <Styled.List>
      {!data ? 'Buscando requerimientos para': <ul> {items} </ul>}
    </Styled.List>
  );
};

export default List;