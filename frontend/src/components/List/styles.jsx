import styled, { css } from 'styled-components';

export const Li = styled.li`
    list-style-type: none;
    padding: 10px;
    box-sizing: border-box;
    height: auto;
    //margin:0 0 3px 0;
    //line-height: 1.5;
    color: #170A1C;
    //border-radius: 5px;
    border-bottom: 1px solid #C9CFD2;
    //background-color: #444;
    // @ts-ignore
    //font-family: 'OpenSans';
    font-size: 75%;
    line-height: 1.5em;

    &:hover {
      background: #E1E5E7;
      //box-sizing: border-box;
      //border-top: 1px solid #BDC3C7;

    }

    ${({ selected }) => selected && `
      background: #1d837a !important;
      color: white;
    `}

    &:last-child {
      margin: 0;
      border-bottom: 0;
    }
    
    //align-self: center;
    //display: flex;
    //height: auto;
    //text-align: left;

    p {
      position: relative;
    display: -webkit-box;
    margin: auto;
    background-color: transparent;
    //color: ${({ theme }) => theme.onSurface};
    font-size: inherit;
    font-family: inherit;
    text-align: left;
    text-overflow: ellipsis;
    line-height: 1.5em;
    overflow: hidden;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    }
    //white-space: nowrap;
`;

const ul = css`
  ul {
    margin: 0;
    padding: 0;
    line-height: 1.6em;
  }
  ${Li}
`;

export const List = styled.div`
  ul {
    margin: 0;
    padding: 0;
  }
  ${Li}
  //background-color: blue;
  padding: 0px;
  //height: 100%;
  width: 300px;
  height: 100%;
  margin: 0;
  box-sizing: border-box;
  overflow: hidden;
  overflow-y: scroll;
  border-radius: 5px;
  background-color: #ECF0F1;
  scrollbar-width: thin;
  scrollbar-width: none;
  &::-webkit-scrollbar {
      display: none;
    }

`;