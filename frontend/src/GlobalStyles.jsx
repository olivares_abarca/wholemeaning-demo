import { createGlobalStyle } from 'styled-components';
import PoppinsLight from "./fonts/Poppins-Light.ttf";
import ArchivoMedium from './fonts/Archivo-Medium.ttf';

export default createGlobalStyle`
  *, body {
    height: 100%;
  }

  * {
    margin: 0 auto;
    font-family: 'Poppins Light';
  }

  body {
    background-color: #ECF0F1;
  }

  @font-face {
    font-family: 'Archivo Medium';
    src: url(${ArchivoMedium}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'Poppins Light';
    src: url(${PoppinsLight}) format('truetype');
    font-weight: lighter;
    font-style: normal;
  }
`;