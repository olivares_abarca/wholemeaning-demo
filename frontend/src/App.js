import { useEffect, useState } from 'react';
import styled from 'styled-components';
import List from './components/List';
import Input from './components/Input';
import Table from './components/Table';
import GlobalStyles from './GlobalStyles';

const Main = styled.div`
  height: 100%;
  width: 100%;
  display: table;
`;

const Wrapper = styled.div`
  display: table-cell;
  height: 100%;
  vertical-align: middle;
`;

const Sectors = styled.div`
  height: auto;
  text-align: center;
  margin-bottom: 30px;
`;

const SectorButton = styled.button`
  border: none;
  font-size: 15px;
  color: black;
  background-color: transparent;
  border-bottom: 2px solid transparent;
  margin: 10px;
  cursor: pointer;
  font-family: 'Archivo Medium';
  //font-weight: 500;
  
  a {
    font-family: inherit;
    color: inherit;
    text-decoration: none;
  }

  ${({ active }) => active && `
      color: #1d837a !important;
      color: white;
      border-bottom: 2px solid #1d837a;
  `}

  transition: 0.2s;
`;

const Container = styled.div`
  height: auto;
  display: flex;
  max-width: 1080px;
  height: 400px;
  padding: 30px;
  border-radius: 10px;
  background-color: white;
  gap: 30px;
`;

const Separator = styled.div`
  border-left: 1px solid #E1E5E7;
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;;

  h3 {
    font-family: 'Archivo Medium';
    height: auto;
    margin: 0;
    padding: 0;
    margin-bottom: 25px;
    font-weight: normal;
  }

  button {
    font-family: 'Archivo Medium';
    height: auto;
    margin-top: 50px;
    align-self: center;
  }
`;

function App() {
  const sectors = ['Banca', 'Seguros', 'Retail'];
  const links = [{name: 'Chat', url: 'https://direct.lc.chat/13401966/'}, {name: 'Flujo', url: 'https://www.figma.com/proto/MiWNGGdaoAwu0LySDDaWc6/Prototipo-Innovacion?page-id=0%3A1&node-id=1%3A65&viewport=244%2C48%2C0.28&scaling=scale-down-width&starting-point-node-id=1%3A65'}];
  const [selectedSector, setSelectedSector] = useState('Banca');
  const [requirements, setRequirements] = useState([]);
  const [selectedRequirement, setSelectedRequirement] = useState('');
  const [response, setResponse] = useState('');
  const [category, setCategory] = useState('');

  const showSectors = () => {
    return sectors.map(sector => <SectorButton active={sector == selectedSector ? true : false} value={sector} onClick={(e) => setSelectedSector(e.target.value)}>{sector}</SectorButton>);
  }

  const showLinks = () => {
    return links.map(link => <SectorButton><a href={link.url} target="_blank">{link.name}</a></SectorButton>)
  }

  useEffect(() => {
    fetch('/requirements').then(response => { return response.json() })
    .then((data) => data.map((item, idx) => ({id: idx, ...item, selected: false})))
    .then((data) => setRequirements(data));
  }, []);

  useEffect(() => {
    const aux = requirements.find(item => item.selected);
    if (aux)
      setSelectedRequirement(aux.message);
  }, [requirements]);

  const getClassification = () => {
    const body = { text: selectedRequirement, sector: selectedSector };
    fetch('/classification', {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => { return response.json() })
    .then((data) => { setResponse(data.template); setCategory(data.name)});
  };

  return (
    <div>
      <Main>
      <GlobalStyles/>
      <Wrapper>
        <Sectors>
          {showSectors()}
          {showLinks()}
        </Sectors>
        <Container>
            <Card>
              <h3>Selecciona un requerimiento</h3>
              <List items={requirements} sector={selectedSector} updateItems={requirements => setRequirements(requirements)}></List>
            </Card>
            <Separator/>
            <Card>
              <h3>Categoriza el texto</h3>
              <Input template={selectedRequirement} updateContent={selectedRequirement => setSelectedRequirement(selectedRequirement)}></Input>
              <button onClick={getClassification}>Categorizar</button>
            </Card>
            <Separator/>
            <Card>
              <h3>Resultados</h3>
              <Table category={category} template={response}></Table>
            </Card>
          </Container>
        </Wrapper>
        </Main>
    </div>
  );
}

export default App;