import axios from 'axios';
import cors from 'cors';
import express from 'express';

const app = express();

app.use(cors({
  origin: '*'
}));

const port : number = Number(3001);


const getRequirements = async () => {
  const response = await axios.get('https://webhook.wholemeaning.com/webhook/tool-demo/requirements');
  return response.data;
}

const getClassification = async (body: any) => {
  const response = await axios.post('https://webhook.wholemeaning.com/webhook/tool-demo/categorize', body, { headers: { 'content-type': 'application/json' }});
  return response.data;
}


app.get('/requirements', async (req, res) => {

  try {
    const response = await getRequirements();
    res.json(response);
  } catch (e) {
    
  }
});

app.post('/classification', async (req, res) => {
    try {
      const response = await getClassification(req);
      res.json(response);
    } catch (e) {
      
    }
  });
  

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});